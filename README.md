# solidity-parser-ocaml

A Solidity parser written in OCaml with Menhir

# Required dependencies

You need the following packages to build the Solidity parser:
- dune
- menhir
- zarith

You may install these dependencies either in your global opam directory,
or run `make build-deps` to create a local switch containing the
required dependencies to build.

# How to build

A simple `make` command will build the parser.

# How to run

Simply type:
`solp contract.sol`

This will parse the file and then display the contract, if correctly parsed.

# Compatibility with the original Solidity language

Our Solidity parser should support most of the Solidity language
(version 0.6), with the notable exception of inline assembly (may
be added in a future release).

# Future improvements

As of now, error messages are pretty basic, so it's hard to tell where
and why a parsing error occurs. The next release will feature improved
error messages.
