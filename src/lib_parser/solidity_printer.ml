(**************************************************************************)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs, Dune Network and OCamlPro                 *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Solidity_types

module Printf = struct
  let bprintf = Printf.bprintf
  let sprintf = Format.sprintf
end

let spaces = String.make 1000 ' '

let bprint b indent s =
  Printf.bprintf b "%s%s\n" (String.sub spaces 0 indent) s

let rec bprint_contract b indent c =
  List.iter (bprint_source_unit b indent) c

and bprint_source_unit b indent = function
  | Pragma (s1, s2) ->
      bprint b indent (Printf.sprintf "pragma %s %s;" s1 s2)
  | Import { import_from; import_symbols = ImportAll (false, id_opt) } ->
      bprint b indent
        (Printf.sprintf "import %s %s;"
           import_from (string_of_maybe_as_identifier id_opt))
  | Import { import_from; import_symbols = ImportAll (true, id_opt) } ->
      bprint b indent
        (Printf.sprintf "import * %s from %s;"
           (string_of_maybe_as_identifier id_opt) import_from)
  | Import { import_from; import_symbols = ImportIdents import_list } ->
      bprint b indent
        (Printf.sprintf "import { %s } from %s;"
           (String.concat ", "
              (List.map (fun (id, id_opt) ->
                   Printf.sprintf "%s %s" (Ident.to_string id)
                     (string_of_maybe_as_identifier id_opt)
                 ) import_list)) import_from)
  | GlobalTypeDefinition td ->
      type_definition b indent td
  | ContractDefinition cd ->
      bprint b indent
        (Printf.sprintf "%s%s %s %s {"
           (if cd.contract_abstract then "abstract " else "")
           (string_of_contract_kind cd.contract_kind)
           cd.contract_name
           (match cd.contract_inheritance with
            | [] -> ""
            | list ->
                Printf.sprintf "is %s"
                  (String.concat ", "
                     (List.map string_of_inheritance_specifier list))));
      List.iter (contract_part b (indent + 2)) cd.contract_parts ;
      bprint b indent "}"

and string_of_maybe_as_identifier = function
  | None -> ""
  | Some id -> Printf.sprintf "as %s" (Ident.to_string id)

and string_of_contract_kind = function
  | KindContract ->
      "contract"
  | KindLibrary ->
      "library"
  | KindInterface ->
      "interface"

and string_of_inheritance_specifier (lid, e_list) =
  Printf.sprintf "%s (%s)"
    (LongIdent.to_string lid)
    (String.concat ", " (List.map string_of_expression e_list))

and type_definition b indent = function
  | EnumDefinition (id, id_list) ->
      bprint b indent (Printf.sprintf "enum %s {" (Ident.to_string id));
      List.iter (fun id -> bprint b (indent + 2) (Ident.to_string id)) id_list;
      bprint b indent "}"
  | StructDefinition (id, var_decl_list) ->
      bprint b indent (Printf.sprintf "struct %s {" (Ident.to_string id));
      List.iter (fun s ->
          bprint b (indent + 2) (string_of_variable_declaration s)
        ) var_decl_list;
      bprint b indent "}"

and contract_part b indent = function
  | FunctionDefinition f -> (
      bprint b indent
        (Printf.sprintf "function %s (%s) %s %s%s"
           (Ident.to_string f.fun_name)
           (String.concat ", " (List.map string_of_function_param f.fun_params))
           (String.concat " " (List.map string_of_modifier f.fun_modifiers))
           (match f.fun_return with
            | None -> ""
            | Some returns ->
                Printf.sprintf "returns (%s)"
                  (String.concat " * "
                     (List.map string_of_function_return returns)))
           (match f.fun_body with None -> ";" | Some _ -> "{")) ;
      match f.fun_body with
      | None -> ()
      | Some body ->
          block b (indent + 2) body;
          bprint b indent "}" )
  | StateVariableDeclaration { var_name; var_type; var_modifiers; var_init } ->
      bprint b indent
        (Printf.sprintf "%s %s %s %s"
           (string_of_type var_type)
           (String.concat " " (List.map string_of_modifier var_modifiers))
           (Ident.to_string var_name)
           (match var_init with
            | None -> ";"
            | Some e -> Printf.sprintf " = %s;" (string_of_expression e)))
  | UsingForDeclaration (lid, t_opt) ->
      bprint b indent
        (Printf.sprintf "using %s %s;" (LongIdent.to_string lid)
           (match t_opt with
            | None -> ""
            | Some t -> Printf.sprintf "for %s" (string_of_type t)))
  | ModifierDefinition { mod_name; mod_params; mod_modifiers; mod_body } ->
      bprint b indent
        (Printf.sprintf "modifier %s %s %s {" mod_name
           (match mod_params with
            | None -> ""
            | Some params ->
                Printf.sprintf "(%s)"
                  (String.concat ", "
                     (List.map string_of_function_param params)))
           (String.concat " "
              (List.map string_of_modifier mod_modifiers)));
      block b (indent + 2) mod_body;
      bprint b indent "}"
  | EventDefinition { event_name; event_params; event_anon } ->
      bprint b indent
        (Printf.sprintf "event %s (%s) %s;" event_name
           (String.concat ", "
              (List.map (fun (t, indexed, id_opt) ->
                   Printf.sprintf "%s %s %s"
                     (string_of_type t)
                     (string_of_indexed indexed)
                     (match id_opt with
                      | None -> ""
                      | Some id -> Ident.to_string id))
                  event_params))
           (string_of_anonymous event_anon))
  | TypeDefinition td ->
      type_definition b indent td

and string_of_function_param (t, loc_opt, id_opt) =
  Printf.sprintf "%s%s %s"
    (string_of_type t)
    (match loc_opt with
     | None -> ""
     | Some loc -> " " ^ string_of_storage_location loc)
    (match id_opt with
     | None -> ""
     | Some id -> Ident.to_string id)

and string_of_function_return (t, loc_opt) =
  Printf.sprintf "%s%s"
    (string_of_type t)
    (match loc_opt with
     | None -> ""
     | Some loc -> " " ^ string_of_storage_location loc)

and string_of_type = function
  | ElementaryType et ->
      string_of_elementary_type et
  | UserDefinedType lid ->
      LongIdent.to_string lid
  | Mapping (et, t) ->
      Printf.sprintf "mapping (%s => %s)"
        (string_of_elementary_type et) (string_of_type t)
  | Array (t, e_opt) ->
      Printf.sprintf "%s [%s]"
        (string_of_type t)
        (match e_opt with
         | None -> ""
         | Some e -> string_of_expression e)
  | FunctionType { fun_type_params; fun_type_modifiers; fun_type_returns } ->
      Printf.sprintf "function (%s) %s %s"
        (String.concat ", " (List.map string_of_function_param fun_type_params))
        (String.concat " " (List.map string_of_modifier fun_type_modifiers))
        (match fun_type_returns with
         | None -> ""
         | Some returns ->
             Printf.sprintf "returns (%s)"
               (String.concat " * "
                  (List.map string_of_function_return returns)))
  | Tuple tl ->
      Printf.sprintf "(%s)"
        (String.concat ", " (List.map string_of_type tl))
  | Type t ->
      Printf.sprintf "type %S" (string_of_type t)
  | RationalConst q ->
      Printf.sprintf "rational_const %s" (string_of_number q)
  | IntegerConst z ->
      Printf.sprintf "int_const %s" (Z.to_string z)
  | LiteralString b ->
      Printf.sprintf "literal_string (%s)" (if b then "valid" else "invalid")

and string_of_modifier = function
  | ModifierInvocation (id, e_list_opt) ->
      Printf.sprintf "%s (%s)" (Ident.to_string id)
        (match e_list_opt with
         | None -> ""
         | Some el -> String.concat ", " (List.map string_of_expression el))
  | ModifierImmutable ->
      "immutable"
  | ModifierConstant ->
      "constant"
  | ModifierPayable ->
      "payable"
  | ModifierExternal ->
      "external"
  | ModifierPublic ->
      "public"
  | ModifierInternal ->
      "internal"
  | ModifierPrivate ->
      "private"
  | ModifierPure ->
      "pure"
  | ModifierView ->
      "view"
  | ModifierVirtual ->
      "virtual"
  | ModifierOverride lid_list ->
      Printf.sprintf "override (%s)"
        (String.concat ", " (List.map LongIdent.to_string lid_list))

and string_of_elementary_type = function
  | TypeBool ->
      "bool"
  | TypeInt (None) ->
      "int"
  | TypeInt (Some size)->
      Printf.sprintf "int%d" size
  | TypeUint (None) ->
      "uint"
  | TypeUint (Some size)->
      Printf.sprintf "uint%d" size
  | TypeFixed (None) ->
      "fixed"
  | TypeFixed (Some (size, dec)) ->
      Format.sprintf "fixed%dx%d" size dec
  | TypeUfixed (None) ->
      "ufixed"
  | TypeUfixed (Some (size, dec)) ->
      Format.sprintf "ufixed%dx%d" size dec
  | TypeAddress (false) ->
      "address"
  | TypeAddress (true) ->
      "address payable"
  | TypeString ->
      "string"
  | TypeBytes (None) ->
      "bytes"
  | TypeBytes (Some size)->
      Printf.sprintf "bytes%d" size
  | TypeVar ->
      "var"

and string_of_expression_option = function
  | None -> ""
  | Some e -> string_of_expression e

and string_of_expression = function
  | BooleanLiteral bool ->
      string_of_bool bool
  | NumberLiteral (q, number_unit_opt, None) ->
      Printf.sprintf "%s%s"
        (string_of_number q)
        (match number_unit_opt with
         | None -> ""
         | Some u -> string_of_number_unit u)
  | NumberLiteral ((n, _), _number_unit_opt, Some _size) ->
      Printf.sprintf "%x" (Z.to_int n)
  | StringLiteral s ->
      Printf.sprintf "\"%S\"" s
  | HexLiteral s ->
      Printf.sprintf "hex\"%s\"" s
  | AddressLiteral addr ->
      Printf.sprintf "%S" addr
  | IdentifierExpression id ->
      Ident.to_string id
  | ImmediateArray el ->
      Printf.sprintf "[%s]"
        (String.concat ", " (List.map string_of_expression el))
  | ArrayAccess (e, None) ->
      Printf.sprintf "%s[]" (string_of_expression e)
  | ArrayAccess (e1, Some e2) ->
      Printf.sprintf "%s[%s]"
        (string_of_expression e1)
        (string_of_expression e2)
  | ArraySlice (tab, e1_opt, e2_opt) ->
      Printf.sprintf "%s[%s:%s]"
        (string_of_expression tab)
        (match e1_opt with None -> "" | Some e -> string_of_expression e)
        (match e2_opt with None -> "" | Some e -> string_of_expression e)
  | PrefixExpression (op, e2) ->
      Printf.sprintf "(%s %s)" op (string_of_expression e2)
  | SuffixExpression (e1, op) ->
      Printf.sprintf "(%s %s)" (string_of_expression e1) op
  | BinaryExpression (e1, op, e2) ->
      Printf.sprintf "(%s %s %s)"
        (string_of_expression e1) op
        (string_of_expression e2)
  | AssignExpression (e1, op, e2) ->
      Printf.sprintf "(%s %s %s)"
        (string_of_expression e1) op
        (string_of_expression e2)
  | FunctionCallExpression (e, args) ->
      Printf.sprintf "%s(%s)"
        (string_of_expression e) (string_of_function_call_arguments args)
  | TupleExpression e_opt_list ->
      Printf.sprintf "(%s)"
        (String.concat ", " (List.map string_of_expression_option e_opt_list))
  | FieldExpression (e, ident) ->
      Printf.sprintf "(%s).%s" (string_of_expression e) (Ident.to_string ident)
  | IfExpression (e1, e2, e3) ->
      Printf.sprintf "(%s ? %s : %s)"
        (string_of_expression e1)
        (string_of_expression e2)
        (string_of_expression e3)
  | NewExpression t ->
      Printf.sprintf "new %s" (string_of_type t)
  | TypeExpression t ->
      Printf.sprintf "%s" (string_of_type t)

and string_of_number = function
  | (n, d) when Z.equal Z.one d ->
      Z.to_string n
  | (n, d) ->
      Printf.sprintf "%s%s" (Z.to_string n) (Z.to_string d)

and string_of_function_call_arguments = function
  | ExpressionList el ->
      String.concat ", " (List.map string_of_expression el)
  | NameValueList id_exp_list ->
      String.concat ","
        (List.map (fun (id, e) ->
             Printf.sprintf "%s: %s"
               (Ident.to_string id) (string_of_expression e)
           ) id_exp_list)

and block b indent stmt_list =
  List.iter (statement b (indent + 2)) stmt_list

and statement b indent = function
  | Continue ->
      bprint b indent "continue;"
  | Break ->
      bprint b indent "break;"
  | Throw ->
      bprint b indent "throw;"
  | Emit (e, args) ->
      bprint b indent
        (Printf.sprintf "emit %s(%s)"
           (string_of_expression e)
           (string_of_function_call_arguments args))
  | PlaceholderStatement ->
      bprint b indent "_;"
  | Return e_opt ->
      bprint b indent
        (Printf.sprintf "return %s;" (string_of_expression_option e_opt))
  | Block statement_list ->
      bprint b indent "{" ;
      block b (indent + 2) statement_list;
      bprint b indent "}"
  | InlineAssemblyStatement ->
      bprint b indent "ASM;"
  | IfStatement (e, s1, s2_opt) -> (
      bprint b indent
        (Printf.sprintf "if (%s)" (string_of_expression e));
      statement b (indent + 2) s1;
      match s2_opt with
      | None -> ()
      | Some s2 ->
          bprint b indent "else" ;
          statement b (indent + 2) s2 )
  | TryStatement (e, return_opt, body, catch_clause_list) ->
      bprint b indent
        (Printf.sprintf "try %s %s{"
           (string_of_expression e)
           (match return_opt with
            | None -> ""
            | Some return_list ->
                Printf.sprintf "returns (%s) "
                  (String.concat " * "
                     (List.map string_of_function_return return_list))));
      block b (indent + 2) body;
      bprint b indent "}";
      List.iter (fun (id_opt, params, body) ->
          let p =
            String.concat ", " (List.map string_of_function_param params) in
          bprint b indent
            (Printf.sprintf "catch %s{"
               (match id_opt, params with
                | None, [] -> ""
                | None, _ -> Printf.sprintf "(%s)" p
                | Some id, _ -> Printf.sprintf "%s(%s)" id p));
          block b (indent + 2) body;
          bprint b indent "}"
        ) catch_clause_list
  | WhileStatement (e, body) ->
      bprint b indent (Printf.sprintf "while (%s)" (string_of_expression e));
      statement b (indent + 2) body
  | ForStatement (s1_opt, e1_opt, e2_opt, s2) ->
      bprint b indent "for (" ;
      (match s1_opt with None -> () | Some s -> statement b (indent + 2) s);
      bprint b (indent + 2)
        (Printf.sprintf "; %s; %s)"
           (string_of_expression_option e1_opt)
           (string_of_expression_option e2_opt));
      statement b (indent + 2) s2
  | DoWhileStatement (s, e) ->
      bprint b indent "do";
      statement b (indent + 2) s;
      bprint b indent (Printf.sprintf "while (%s)" (string_of_expression e))
  | VariableDefinition vardef ->
      bprint b indent
        (Printf.sprintf "%s;" (string_of_variable_definition vardef))
  | ExpressionStatement e ->
      bprint b indent (Printf.sprintf "%s;" (string_of_expression e))

and string_of_variable_definition = function
  | VarInfer (id_opt_list, e_opt) ->
      Printf.sprintf "var %s%s"
        (String.concat ", "
           (List.map (function None -> "" | Some s -> s) id_opt_list))
        (match e_opt with
         | None -> ""
         | Some e -> Printf.sprintf " = %s" (string_of_expression e))
  | VarType (var_decl_list, e_opt) ->
      let var_decls = match var_decl_list with
        | [Some variable_declaration] ->
            string_of_variable_declaration variable_declaration
        | _ ->
            Printf.sprintf "(%s)"
              (String.concat ", "
                 (List.map (function
                      | None -> ""
                      | Some var_decl -> string_of_variable_declaration var_decl
                    ) var_decl_list))
      in
      Printf.sprintf "%s%s" var_decls
        (match e_opt with
          | None -> ""
          | Some e -> Printf.sprintf " = %s" (string_of_expression e))

and string_of_indexed = function
  | Indexed ->
      "indexed"
  | NotIndexed ->
      ""

and string_of_anonymous = function
  | Anonymous ->
      "anonymous"
  | NotAnonymous ->
      ""

and string_of_number_unit = function
  | Ether ->
      "ether"
  | Wei ->
      "wei"
  | Szabo ->
      "szabo"
  | Finney ->
      "finney"
  | Seconds ->
      "seconds"
  | Minutes ->
      "minutes"
  | Hours ->
      "hours"
  | Days ->
      "days"
  | Weeks ->
      "weeks"
  | Years ->
      "years"

and string_of_variable_declaration = function
  | (t, loc_opt, id) ->
      Printf.sprintf "%s%s %s"
        (string_of_type t)
        (match loc_opt with
        | None -> ""
        | Some loc -> " " ^ string_of_storage_location loc)
        (Ident.to_string id)

and string_of_storage_location = function
  | Memory ->
      "memory"
  | Storage ->
      "storage"
  | Calldata ->
      "calldata"

let string_of_code code =
  let b = Buffer.create 1000 in
  let indent = 0 in
  bprint_contract b indent code;
  Buffer.contents b
