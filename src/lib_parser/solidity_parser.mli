(**************************************************************************)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs, Dune Network and OCamlPro                 *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or         *)
(*  modify it under the terms of the GNU Lesser General Public License    *)
(*  published by the Free Software Foundation, either version 3 of        *)
(*  the License, or any later version.                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU Lesser                     *)
(*  General Public License along with this program.                       *)
(*  If not, see <https://www.gnu.org/licenses/>.                          *)
(*                                                                        *)
(**************************************************************************)

module Printer : module type of Solidity_printer
module Types : module type of Solidity_types

val parse_contract_file : string -> Types.module_

val parse_contract_string : string -> Types.module_
