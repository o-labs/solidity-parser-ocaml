(**************************************************************************)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs, Dune Network and OCamlPro                 *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

exception SyntaxError of string

let error fmt = Format.kasprintf Pervasives.failwith fmt

module IntMap = struct
  include Map.Make (struct
    type t = int
    let compare = Stdlib.compare
  end)

  let of_bindings list =
    List.fold_left (fun map (key, v) -> add key v map) empty list
end

module StringMap = struct
  include Map.Make (String)

  let of_bindings list =
    List.fold_left (fun map (key, v) -> add key v map) empty list

  let add_uniq key v map =
    if mem key map then error "StringMap.add_unit %S: not uniq" key ;
    add key v map
end

module Ident = struct
  type t = string
  let compare = String.compare
  let equal = String.equal
  let to_string id = id
end

module LongIdent = struct
  type t = Ident.t list
  let rec compare lid1 lid2 =
    match lid1, lid2 with
    | [], [] -> 0
    | [], _ :: _ -> -1
    | _ :: _, [] -> 1
    | id1 :: lid1, id2 :: lid2 ->
        let res = Ident.compare id1 id2 in
        if res <> 0 then res
        else compare lid1 lid2
  let equal lid1 lid2 =
    compare lid1 lid2 = 0
  let to_string lid =
    String.concat "." lid
end

module IdentMap = struct
  include Map.Make (Ident)

  let of_bindings list =
    List.fold_left (fun map (key, v) -> add key v map) empty list

  let add_uniq key v map =
    if mem key map then error "IdentMap.add_unit %S: not uniq" key ;
    add key v map
end

module IdentSet = Set.Make (Ident)

module LongIdentMap = Map.Make (LongIdent)

module LongIdentSet = Set.Make (LongIdent)

type program = (string * module_) list

and module_ = source_unit list

and source_unit =
  | Pragma of (Ident.t * string)
  | Import of import_directive
  | ContractDefinition of contract_definition
  | GlobalTypeDefinition of type_definition

and import_directive = {
  import_from : string;
  import_symbols : import_symbols;
}

and import_symbols =
  | ImportAll of bool * Ident.t option (* true if using star notation *)
  | ImportIdents of (Ident.t * Ident.t option) list

and type_definition =
  | StructDefinition of Ident.t * variable_declaration list
  | EnumDefinition of Ident.t * Ident.t list

and contract_definition = {
  contract_abstract : bool;
  contract_kind : contract_kind;
  contract_name : Ident.t;
  contract_inheritance : inheritance_specifier list;
  contract_parts : contract_part list;
}

and contract_kind =
  | KindContract
  | KindLibrary
  | KindInterface

and inheritance_specifier = LongIdent.t * expression list

and contract_part =
  | UsingForDeclaration of LongIdent.t * type_ option
  | TypeDefinition of type_definition
  | StateVariableDeclaration of state_variable_definition
  | FunctionDefinition of function_definition
  | ModifierDefinition of modifier_definition
  | EventDefinition of event_definition

and state_variable_definition = {
  var_name : Ident.t;
  var_type : type_;
  var_modifiers : modifier list;
  var_init : expression option;
}

and function_definition = {
  fun_name : Ident.t;
  fun_params : param list;
  fun_modifiers : modifier list;
  fun_return : return list option;
  fun_body : block option;
}

and modifier_definition = {
  mod_name : Ident.t;
  mod_params : param list option;
  mod_modifiers : modifier list; (* only virtual/override *)
  mod_body : block;
}

and event_definition = {
  event_name : Ident.t;
  event_params : (type_ * indexed * Ident.t option) list;
  event_anon : anonymous;
}

and param = type_ * storage_location option * Ident.t option

and return = type_ * storage_location option

and modifier =
  | ModifierPayable
  | ModifierView
  | ModifierPure
  | ModifierPublic
  | ModifierExternal
  | ModifierInternal
  | ModifierPrivate
  | ModifierConstant
  | ModifierImmutable
  | ModifierVirtual
  | ModifierOverride of LongIdent.t list
  | ModifierInvocation of Ident.t * expression list option

and function_type = {
  fun_type_params : param list;
  fun_type_modifiers : modifier list;
  fun_type_returns : return list option
}

and elementary_type =
  | TypeBool
  | TypeInt of int option (* None = int, Some (N) = intN *)
  | TypeUint of int option (* None = uint, Some (N) = uintN *)
  | TypeFixed of (int * int) option (* None = fixed, Some (M,N) = fixedMxN *)
  | TypeUfixed of (int * int) option (* None = ufixed, Some (M,N) = ufixedMxN *)
  | TypeString
  | TypeBytes of int option (* None = bytes, Some (N) = bytesN *)
  | TypeAddress of bool (* false = address, true = address payable *)
  | TypeVar

and type_ =
  | UserDefinedType of LongIdent.t
  | ElementaryType of elementary_type
  | Mapping of elementary_type * type_
  | Array of type_ * expression option
  | FunctionType of function_type

  (* Only at typecheck/runtime *)
  | Tuple of type_ list
  | Type of type_
  | IntegerConst of Z.t
  | RationalConst of number
  | LiteralString of bool (* true if valid UTF-8 string *)

and statement =
  | VariableDefinition of variable_definition
  | ExpressionStatement of expression
  | Block of block
  | IfStatement of expression * statement * statement option
  | WhileStatement of expression * statement
  | DoWhileStatement of statement * expression
  | ForStatement of
      statement option * expression option * expression option * statement
  | TryStatement of expression * return list option * block * catch_clause list
  | InlineAssemblyStatement (* Unsupported *)
  | Return of expression option
  | Continue
  | Break
  | Throw
  | Emit of expression * function_call_arguments
  | PlaceholderStatement

and expression =
  | BooleanLiteral of bool
  | NumberLiteral of number * number_unit option * int option (* hex size *)
  | StringLiteral of string
  | HexLiteral of string
  | AddressLiteral of string
  | IdentifierExpression of Ident.t
  | ImmediateArray of expression list
  | ArrayAccess of expression * expression option
  | ArraySlice of expression * expression option * expression option
  | PrefixExpression of string * expression
  | SuffixExpression of expression * string
  | BinaryExpression of expression * string * expression
  | AssignExpression of expression * string * expression
  | FunctionCallExpression of expression * function_call_arguments
  | TupleExpression of expression option list
  | FieldExpression of expression * Ident.t
  | IfExpression of expression * expression * expression
  | NewExpression of type_
  | TypeExpression of type_

and function_call_arguments =
  | ExpressionList of expression list
  | NameValueList of (Ident.t * expression) list

and block = statement list

and catch_clause = Ident.t option * param list * block

and variable_definition =
  | VarInfer of Ident.t option list * expression option
  | VarType of variable_declaration option list * expression option

and variable_declaration = type_ * storage_location option * Ident.t

and storage_location = Memory | Storage | Calldata

and indexed = Indexed | NotIndexed

and anonymous = Anonymous | NotAnonymous

and number = Z.t * Z.t (* num / den *)

and number_unit =
  | Ether
  | Wei
  | Szabo
  | Finney
  | Seconds
  | Minutes
  | Hours
  | Days
  | Weeks
  | Years
