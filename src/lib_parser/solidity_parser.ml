(**************************************************************************)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs, Dune Network and OCamlPro                 *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or         *)
(*  modify it under the terms of the GNU Lesser General Public License    *)
(*  published by the Free Software Foundation, either version 3 of        *)
(*  the License, or any later version.                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU Lesser                     *)
(*  General Public License along with this program.                       *)
(*  If not, see <https://www.gnu.org/licenses/>.                          *)
(*                                                                        *)
(**************************************************************************)

module Printer = Solidity_printer
module Types = Solidity_types

let parse_contract_file file =
  let c = open_in file in
  let lb = Lexing.from_channel c in
  let code = Parser.top_module Lexer.token lb in
  close_in c;
  code

let parse_contract_string string =
  let lb = Lexing.from_string string in
  Parser.top_module Lexer.token lb
