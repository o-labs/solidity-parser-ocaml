(**************************************************************************)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs, Dune Network and OCamlPro                 *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or         *)
(*  modify it under the terms of the GNU Lesser General Public License    *)
(*  published by the Free Software Foundation, either version 3 of        *)
(*  the License, or any later version.                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU Lesser                     *)
(*  General Public License along with this program.                       *)
(*  If not, see <https://www.gnu.org/licenses/>.                          *)
(*                                                                        *)
(**************************************************************************)

let main () =
  let arg_list = Arg.align [
      "--version", Arg.Unit (fun () ->
          Format.eprintf "Solidity Parser v0.1"; exit 0),
      " Show version and exit";
    ]
  in

  let arg_usage = String.concat "\n" [
      "solp [OPTIONS] FILE [OPTIONS]";
      "";
      "This tool will parse a Solidity file (.solc) and print the result";
      "";
      "Available options:";
    ]
  in
  let file = ref None in
  Arg.parse arg_list (fun a ->
      Format.eprintf "Argument %s@." a;
      if !file <> None then begin
        Format.eprintf "More than one file specified";
        exit 1
      end;
      file := Some a;
    ) arg_usage;
  match !file with
  | None ->
      Arg.usage arg_list arg_usage;
      exit 1
  | Some file ->
      let code = Solidity_parser.parse_contract_file file in
      Format.printf "Parsed code: %s@."
        (Solidity_parser.Printer.string_of_code code)

let () =
  main ()
