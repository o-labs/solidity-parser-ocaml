
all:
	eval $(opam env)
	dune build
	dune build src/bin_solp/solp.exe
	cp -f _build/default/src/bin_solp/solp.exe ./solp

clean:
	rm -rf _obuild/*
	rm -f solp

build-deps:
	./build_deps.sh
