#!/bin/bash

which "opam" > /dev/null

if [ $? -ne 0 ] ; then
  echo 'OPAM not found. Please install opam by executing:'
  echo 'sh <(curl -sL https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh)'
  exit 1
fi

opam switch create --deps-only -y ./src/lib_parser ocaml-base-compiler.4.07.1
